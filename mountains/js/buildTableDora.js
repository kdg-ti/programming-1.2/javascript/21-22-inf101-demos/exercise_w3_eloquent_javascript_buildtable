const MOUNTAINS = [
    {name: "Kilimanjaro", height: 5895, place: "Tanzania"},
    {name: "Everest", height: 8848, place: "Nepal"},
    {name: "Mount Fuji", height: 3776, place: "Japan"},
    {name: "Vaalserberg", height: 323, place: "Netherlands"},
    {name: "Denali", height: 6168, place: "United States"},
    {name: "Popocatepetl", height: 5465, place: "Mexico"},
    {name: "Mont Blanc", height: 4808, place: "Italy/France"}
];

tableString = "";

tableString += "<table>\n" +
    "  <tr>\n";
for (let i = 0; i<Object.keys(MOUNTAINS[0]).length; i++){
    key = Object.keys(MOUNTAINS[0])[i];
    tableString += "<th>" + key + "</th>\n";
}
tableString +=     "  </tr>\n";
for (let i = 0; i<MOUNTAINS.length; i++){
    tableString += 
    "<tr>\n" + 
    "<td>"+ MOUNTAINS[i].name + "</td>\n" +
    "<td class='numeric'>"+ MOUNTAINS[i].height + "</td>\n" +
    "<td>"+ MOUNTAINS[i].place + "</td>\n" + "</tr>\n"
};
tableString += "</table>"
elem = document.getElementById("mountains");
console.log(tableString);
elem.innerHTML = tableString;

numericList = document.getElementsByClassName("numeric");
for (let i = 0; i < numericList.length; i++){
    numericList[i].style.textAlign = "right";
}