const MOUNTAINS = [
    {name: "Kilimanjaro", height: 5895, place: "Tanzania"},
    {name: "Everest", height: 8848, place: "Nepal"},
    {name: "Mount Fuji", height: 3776, place: "Japan"},
    {name: "Vaalserberg", height: 323, place: "Netherlands"},
    {name: "Denali", height: 6168, place: "United States"},
    {name: "Popocatepetl", height: 5465, place: "Mexico"},
    {name: "Mont Blanc", height: 4808, place: "Italy/France"}
];

function showMountains() {
    const fields = Object.keys(MOUNTAINS[0]);
    tableString = "";

    tableString += "<table>\n"
    // for (let i = 0; i < fields.length; i++) {
    //     key = Object.keys(MOUNTAINS[0])[i];
    //     tableString += `<th> ${key} </th>\n`;
    // }

    tableString += fields.reduce((result,key) => result+=`<th> ${key} </th>\n`,
      "<tr>\n");
    tableString += "  </tr>\n";

    for (let i = 0; i < MOUNTAINS.length; i++) {
        tableString += "<tr>\n"
        for (const field of fields){
            const cell = MOUNTAINS[i][field]
            let style
            if (isNaN(cell)) {
                style = ""
            } else {
                style = " class='numeric-cell'"
              //  style = " style='text-align:right;'"
            }
            tableString += `<td${style}>${cell}</td>\n`
        }
        tableString += "</tr>\n"
    }
    tableString += "</table>"
    elem = document.getElementById("mountains");
    console.log(tableString);
    elem.innerHTML = tableString;

    numericList = document.getElementsByClassName("numeric");
    for (let i = 0; i < numericList.length; i++) {
        numericList[i].style.textAlign = "right";
    }
}

showMountains()